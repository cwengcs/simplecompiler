.globl _Main_main
_Main_main:
mov $0, %eax
loop:
inc %eax
cmp $10, %eax
jl loop
ret


# push $1 #first arg
# push $2 #second arg
# pop %eax #second arg
# pop %ebx #first arg
# cmp %eax, %ebx # 2 (second arg) ? 1 (first arg)
# jle true
# false:
# push $0
# jmp end
# true:
# push $1
# end:
# pop %eax
# ret
