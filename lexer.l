%option yylineno
%pointer

%{
    #include <stdlib.h>
    #include <errno.h>
    #include <limits.h>
    #include "ast.hpp"
    #include "parser.hpp"
    void yyerror(const char *);
    extern astnode_union yylval;
%}

letter [a-zA-Z]
integer 0|[1-9][0-9]*
identifier {letter}({letter}|[0-9])* 

%x comment

%%

  /* WRITEME: Copy your Flex rules from Project 3 here */
"/*"                              BEGIN(comment);
<comment>[^*\n]*
<comment>"*"+[^*/\n]*
<comment>\n
<comment><<EOF>>                  { yyerror("dangling comment"); }                     
<comment>"*/"                     BEGIN(INITIAL);
"print"                           { return T_PRINT;       }
"return"                          { return T_RETURN;      }
"if"                              { return T_IF;          }
"else"                            { return T_ELSE;        }
"for"                             { return T_FOR;         }
"new"                             { return T_NEW;         }
"or"                              { return T_OR;          }
"and"                             { return T_AND;         }
"not"                             { return T_NOT;         }
"true"                            { yylval.base_int = 1; return T_TRUE;        }
"false"                           { yylval.base_int = 0; return T_FALSE;       }
"none"                            { return T_NONE;        }
"int"                             { return T_I;           }
"bool"                            { return T_BOOL;        }
"."                               { return T_DOT;         }
"+"                               { return T_PLUS;        }
"-"                               { return T_MINUS;       }
"*"                               { return T_MULTIPLY;    }
"/"                               { return T_DIVIDE;      }
"="                               { return T_ASSIGN;      }
"<"                               { return T_LT;          }
"<="                              { return T_LTE;         }
"=="                              { return T_EQ;          }
"("                               { return T_OPENPAREN;   }
")"                               { return T_CLOSEDPAREN; }
"{"                               { return T_OPENBRACE;   }
"}"                               { return T_CLOSEDBRACE; }
","                               { return T_COMMA;       }
":"                               { return T_COLON;       }
";"                               { return T_SEMICOLON;   }
{integer}                         { yylval.base_int = atoi(yytext); return T_INT;           }
{identifier}                      { yylval.base_char_ptr = strdup(yytext); return T_ID;          }
[ \t\n]
.                                 { yyerror("invalid character"); }

%%

int yywrap(void) {
  return 1;
}
