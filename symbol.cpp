#include "symbol.hpp"

// Symbol Visitor Functions: These are the functions you will
// complete to build the symbol table. Not all functions must
// have code, many may be left empty.


void Symbol::visitProgramNode(ProgramNode* node) {
    classTable = new ClassTable();
    node->visit_children(this);
}

void Symbol::visitClassNode(ClassNode* node) {

    /* constructing class info for insertion */ 
    ClassInfo someClass;
    /* current values */
    currentMemberOffset = 0;
    currentLocalOffset = 0;
    currentMethodTable = &(someClass.methods);
    currentVariableTable = &(someClass.members);
    /* adding superclass variables to members of its subclass */
    if (node->identifier_2!=NULL) {
      someClass.superClassName = node->identifier_2->name;
      for (VariableTable::iterator it=((*classTable)[someClass.superClassName].members).begin(); it != ((*classTable)[someClass.superClassName].members).end(); it++)
      {
        someClass.members.insert(std::pair<std::string,VariableInfo>(it->first,it->second));
        currentMemberOffset+=4;
      }
      // for (MethodTable::iterator it=((*classTable)[someClass.superClassName].methods).begin(); it != ((*classTable)[someClass.superClassName].methods).end(); it++)
      // {
      //   someClass.methods.insert(std::pair<std::string,MethodInfo>(it->first,it->second));
      // }
    } 
    else
    {
      someClass.superClassName = "";
    }
    someClass.membersSize = 0;
    /* visit children */
    node->visit_children(this);
    /* setting size for members of the class */
    someClass.membersSize = someClass.members.size()*4;
    /* table insertion */
    classTable->insert(std::pair<std::string,ClassInfo>(node->identifier_1->name,someClass));
}

void Symbol::visitMethodNode(MethodNode* node) {
    /* constructing method info for insertion */ 
    MethodInfo someMethod;
    someMethod.returnType.baseType = node->type->basetype;
    if (node->type->basetype==bt_object) someMethod.returnType.objectClassName = ((ObjectTypeNode*)(node->type))->identifier->name;
    someMethod.localsSize = 0;
    /* current values */
    currentParameterOffset = 12;
    currentVariableTable = &(someMethod.variables);
    /* visit children */
    node->visit_children(this);
    /* setting size of method */
    someMethod.localsSize = (currentLocalOffset+4)*-1;
    /* table insertion */
    currentMethodTable->insert(std::pair<std::string,MethodInfo>(node->identifier->name,someMethod));
}

void Symbol::visitMethodBodyNode(MethodBodyNode* node) {
    currentLocalOffset = -4;
    node->visit_children(this);
}

void Symbol::visitParameterNode(ParameterNode* node) {
    /* constructing variable info */
    VariableInfo someVariable;
    someVariable.type.baseType = node->type->basetype;
    if (node->type->basetype==bt_object) someVariable.type.objectClassName = ((ObjectTypeNode*)(node->type))->identifier->name;
    someVariable.offset = currentParameterOffset;
    someVariable.size = 4;
    /* incrementing current param offset */ 
    currentParameterOffset += 4;
    /* visiting children to get information first */
    node->visit_children(this);
    /* table insertion */
    currentVariableTable->insert(std::pair<std::string,VariableInfo>(node->identifier->name,someVariable));
}

void Symbol::visitDeclarationNode(DeclarationNode* node) {
    std::list<IdentifierNode*> idList = *(node->identifier_list);
    for (std::list<IdentifierNode*>::const_iterator iterator = idList.begin(), end = idList.end(); iterator != end; ++iterator) {
        /* constructing variable info */
        VariableInfo someVariable;
        someVariable.type.baseType = node->type->basetype;
        if (node->type->basetype==bt_object) someVariable.type.objectClassName = ((ObjectTypeNode*)(node->type))->identifier->name;
        if (!currentLocalOffset) {
            someVariable.offset = currentMemberOffset;
            currentMemberOffset += 4;
        }
        else {
            someVariable.offset = currentLocalOffset;
            currentLocalOffset -= 4;
        }
        someVariable.size = 4;
        /* table insertion */
        currentVariableTable->insert(std::pair<std::string,VariableInfo>((*iterator)->name,someVariable));
    }
}

void Symbol::visitReturnStatementNode(ReturnStatementNode* node) {
}

void Symbol::visitAssignmentNode(AssignmentNode* node) {
}

void Symbol::visitCallNode(CallNode* node) {
}

void Symbol::visitIfElseNode(IfElseNode* node) {
}

void Symbol::visitForNode(ForNode* node) {
}

void Symbol::visitPrintNode(PrintNode* node) {
}

void Symbol::visitPlusNode(PlusNode* node) {
}

void Symbol::visitMinusNode(MinusNode* node) {
}

void Symbol::visitTimesNode(TimesNode* node) {
}

void Symbol::visitDivideNode(DivideNode* node) {
}

void Symbol::visitLessNode(LessNode* node) {
}

void Symbol::visitLessEqualNode(LessEqualNode* node) {
}

void Symbol::visitEqualNode(EqualNode* node) {
}

void Symbol::visitAndNode(AndNode* node) {
}

void Symbol::visitOrNode(OrNode* node) {
}

void Symbol::visitNotNode(NotNode* node) {
}

void Symbol::visitNegationNode(NegationNode* node) {
}

void Symbol::visitMethodCallNode(MethodCallNode* node) {
}

void Symbol::visitMemberAccessNode(MemberAccessNode* node) {
}

void Symbol::visitVariableNode(VariableNode* node) {
}

void Symbol::visitIntegerLiteralNode(IntegerLiteralNode* node) {
}

void Symbol::visitBooleanLiteralNode(BooleanLiteralNode* node) {
}

void Symbol::visitNewNode(NewNode* node) {
}

void Symbol::visitIntegerTypeNode(IntegerTypeNode* node) {
}

void Symbol::visitBooleanTypeNode(BooleanTypeNode* node) {
}

void Symbol::visitObjectTypeNode(ObjectTypeNode* node) {
}

void Symbol::visitNoneNode(NoneNode* node) {
}

void Symbol::visitIdentifierNode(IdentifierNode* node) {
}

void Symbol::visitIntegerNode(IntegerNode* node) {
}
