.globl _Main_main
myfunc:
push %ebp
mov %esp, %ebp
mov 8(%ebp), %eax
mov 12(%ebp), %ebx
add %ebx, %eax
push %eax
pop %eax
mov %ebp, %esp
pop %ebp
ret
_Main_main:
# push %eax
push $5
push $10
call myfunc
# pop %eax
add $8, %esp
push %eax
pop %eax
ret

# represents
# myfunc(int a, int b) : int {
#   return a + b
# }
# main() : int {
#   return myfunc(5,10)
# }
