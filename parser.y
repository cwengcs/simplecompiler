%{
    #include <stdio.h>
    #include <stdlib.h>
    #include <iostream>
    #include "ast.hpp"
    
    #define YYDEBUG 1
    int yylex(void);
    void yyerror(const char *);
    
    extern ASTNode* astRoot;
%}

%error-verbose

/* WRITEME: Copy your token and precedence specifiers from Project 3 here */
%token T_PRINT
%token T_RETURN
%token T_IF T_ELSE T_FOR
%token T_NEW
%token T_OR T_AND
%token T_NOT
%token T_TRUE T_FALSE
%token T_NONE
%token T_INT
%token T_BOOL
%token T_I
%token T_ID
%token T_PLUS T_MINUS 
%token T_MULTIPLY T_DIVIDE
%token T_LT T_LTE T_EQ
%token T_OPENPAREN T_CLOSEDPAREN
%token T_OPENBRACE T_CLOSEDBRACE
%token T_COMMA
%token T_DOT
%token T_COLON
%token T_ASSIGN
%token T_SEMICOLON

%left T_OR
%left T_AND
%left T_LT T_LTE T_EQ
%left T_PLUS T_MINUS
%left T_MULTIPLY T_DIVIDE
%right T_NOT UMINUS

/* WRITEME: Specify types for all nonterminals and necessary terminals here */

/* Commonly used Tokens */
%type <type_ptr> Type ReturnType
%type <integertype_ptr> T_I
%type <booleantype_pter> T_BOOL
%type <base_char_ptr> T_ID
%type <base_int> T_INT T_TRUE T_FALSE

/* Types for the "Expression Subset" */
%type <expression_list_ptr> Parameters P
%type <expression_ptr> Expression
%type <methodcall_ptr> MethodCall

/* Types for the "Statement Subset" */
%type <statement_list_ptr> Statements Block
%type <statement_ptr> Statement
%type <assignment_ptr> Assignment
%type <ifelse_ptr>  IfElse
%type <print_ptr> Print
%type <for_ptr> ForLoop

/* Types for the Declarations and Members Subset */
%type <declaration_list_ptr> Declarations Members
%type <identifier_list_ptr> MultipleDeclarations
%type <declaration_ptr> Declaration Member

/* Types for Body */
%type <returnstatement_ptr> Return
%type <methodbody_ptr> Body

/* Types for Methods */
%type <method_list_ptr> Methods
%type <method_ptr> Method
%type <parameter_list_ptr> Arguments A
%type <parameter_ptr> Argument

/* Types for Classes */
%type <class_list_ptr> Classes
%type <class_ptr> Class
%type <program_ptr> Start
%%

/* WRITEME: This rule is a placeholder. Replace it with your grammar
            rules from Project 3 */
Start                        : Classes { $$ = new ProgramNode($1); 
                                        astRoot = $$; }
                             ;
Classes                      : Class { $$ = new std::list<ClassNode*>(); $$->push_back($1); }
                             | Classes Class { $$ = $1; $$->push_back($2); }
                             ;
Class                        : T_ID T_COLON T_ID T_OPENBRACE Members Methods T_CLOSEDBRACE { $$ = new ClassNode(new IdentifierNode($1), new IdentifierNode($3), $5, $6); }
                             | T_ID T_OPENBRACE Members Methods T_CLOSEDBRACE { $$ = new ClassNode(new IdentifierNode($1), NULL, $3, $4); }
                             ;
Members                      : Members Member { $$ = $1;  $$->push_back($2); }
                             | { $$ = new std::list<DeclarationNode*>(); }
                             ;
Member                       : Type T_ID { $$ = new DeclarationNode($1, new std::list<IdentifierNode*>(1, new IdentifierNode($2))); }
                             ;
Type                         : T_I { $$ = new IntegerTypeNode(); $$->basetype = bt_integer;}
                             | T_BOOL { $$ = new BooleanTypeNode(); $$->basetype = bt_boolean; }
                             | T_ID { $$ = new ObjectTypeNode(new IdentifierNode($1)); $$->basetype = bt_object; }
                             ;
Methods                      : Method Methods { $$ = $2; $$->push_front($1); }
                             | { $$ = new std::list<MethodNode*>(); }
                             ;
Method                       : T_ID T_OPENPAREN Arguments T_CLOSEDPAREN T_COLON ReturnType T_OPENBRACE Body T_CLOSEDBRACE { $$ = new MethodNode(new IdentifierNode($1), $3, $6, $8); }
                             ;
Arguments                    : A { $$ = $1; }
                             | { $$ = new std::list<ParameterNode*>(); }
                             ;
A                            : A T_COMMA Argument { $$ = $1; $$->push_back($3); }
                             | Argument { $$ = new std::list<ParameterNode*>(); $$->push_back($1); }
                             ;
Argument                     : Type T_ID { $$ = new ParameterNode($1, new IdentifierNode($2)); }
                             ;
ReturnType                   : Type { $$ = $1; }
                             | T_NONE { $$ = new NoneNode(); $$->basetype = bt_none; }
                             ;
Body                         : Declarations Statements Return { $$ = new MethodBodyNode($1, $2, $3); }
                             ;
Return                       : T_RETURN Expression { $$ = new ReturnStatementNode($2); }
                             | { $$ = NULL; }
                             ;
Declarations                 : Declarations Declaration { $$ = $1; $$->push_back($2); }
                             | { $$ = new std::list<DeclarationNode*>(); }
                             ;
Declaration                  : Type MultipleDeclarations { $$ = new DeclarationNode($1, $2); }
                             ;
MultipleDeclarations         : MultipleDeclarations T_COMMA T_ID { $$ = $1; $$->push_back(new IdentifierNode($3)); }
                             | T_ID { $$ = new std::list<IdentifierNode*>(); $$->push_back(new IdentifierNode($1)); }
                             ;
Statements                   : Statement Statements { $$ = $2; $$->push_front($1); }
                             | { $$ = new std::list<StatementNode*>(); }
                             ;                             
Statement                    : Assignment { $$ = $1; }
                             | MethodCall { $$ = new CallNode($1); }
                             | IfElse { $$ = $1; }
                             | ForLoop { $$ = $1; }
                             | Print { $$ = $1; }
                             ;
Print                        : T_PRINT Expression { $$ = new PrintNode($2); }
                             ;
Assignment                   : T_ID T_ASSIGN Expression { $$ = new AssignmentNode(new IdentifierNode($1), $3); }
                             ;
IfElse                       : T_IF Expression T_OPENBRACE Block T_CLOSEDBRACE { $$ = new IfElseNode($2, $4, NULL); }
                             | T_IF Expression T_OPENBRACE Block T_CLOSEDBRACE T_ELSE T_OPENBRACE Block T_CLOSEDBRACE { $$ = new IfElseNode($2, $4, $8); }
                             ;
ForLoop                      : T_FOR Assignment T_SEMICOLON Expression T_SEMICOLON Assignment T_OPENBRACE Block T_CLOSEDBRACE { $$ = new ForNode($2, $4, $6, $8); }
                             ;
Block                        : Statements { $$ = $1; }
                             ;
Expression                   : Expression T_PLUS Expression     { $$ = new PlusNode($1, $3); }
                             | Expression T_MINUS Expression    { $$ = new MinusNode($1, $3); }
                             | Expression T_MULTIPLY Expression { $$ = new TimesNode($1, $3); }
                             | Expression T_DIVIDE Expression   { $$ = new DivideNode($1, $3); }
                             | Expression T_LT Expression       { $$ = new LessNode($1, $3); }
                             | Expression T_LTE Expression      { $$ = new LessEqualNode($1, $3); }
                             | Expression T_EQ Expression       { $$ = new EqualNode($1, $3); }
                             | Expression T_AND Expression      { $$ = new AndNode($1, $3); }
                             | Expression T_OR Expression       { $$ = new OrNode($1, $3); }
                             | T_NOT Expression                 { $$ = new NotNode($2); }
                             | T_MINUS Expression %prec UMINUS  { $$ = new NegationNode($2); }
                             | T_ID                             { $$ = new VariableNode(new IdentifierNode($1)); }
                             | T_ID T_DOT T_ID                  { $$ = new MemberAccessNode(new IdentifierNode($1), new IdentifierNode($3)); }
                             | MethodCall                       { $$ = $1; }
                             | T_OPENPAREN Expression T_CLOSEDPAREN { $$ = $2; }
                             | T_INT                            { $$ = new IntegerLiteralNode(new IntegerNode($1)); }
                             | T_TRUE                           { $$ = new BooleanLiteralNode(new IntegerNode($1)); }
                             | T_FALSE                          { $$ = new BooleanLiteralNode(new IntegerNode($1)); }
                             | T_NEW T_ID                       { $$ = new NewNode(new IdentifierNode($2), NULL); }
                             | T_NEW T_ID T_OPENPAREN Parameters T_CLOSEDPAREN { $$ = new NewNode(new IdentifierNode($2), $4); }
                             ;

MethodCall                   : T_ID T_OPENPAREN Parameters T_CLOSEDPAREN { $$ = new MethodCallNode(new IdentifierNode($1), NULL, $3); }
                             | T_ID T_DOT T_ID T_OPENPAREN Parameters T_CLOSEDPAREN { $$ = new MethodCallNode(new IdentifierNode($1), new IdentifierNode($3), $5); }
                             ;
Parameters                   : P { $$ = $1;   }
                             |   { $$ = new std::list<ExpressionNode*>(); }
                             ;
P                            : P T_COMMA Expression { $$ = $1; $$->push_back($3);}
                             | Expression           { $$ = new std::list<ExpressionNode*>(); $$->push_back($1); }
                             ;

%%

extern int yylineno;

void yyerror(const char *s) {
  fprintf(stderr, "%s at line %d\n", s, yylineno);
  exit(0);
}
