#include "codegeneration.hpp"
#if __APPLE__
    // Generate label with leading underscore
    const std::string underscore = "_";
#else
    // Generate without leading underscore
    const std::string underscore = "";
#endif
// CodeGenerator Visitor Functions: These are the functions
// you will complete to generate the x86 assembly code. Not
// all functions must have code, many may be left empty.
//
// NOTE: You only need to complete code for expressions,
// assignments, returns, and local variable space allocation.
// Refer to project description for exact details.

void CodeGenerator::visitProgramNode(ProgramNode* node) {
    std::cout << ".data" << std::endl;
    std::cout << "printstr: .asciz \"%d\\n\"" << std::endl;
    std::cout << ".text" << std::endl;
    std::cout << ".globl " << underscore << "Main_main" << std::endl;
    node->visit_children(this);
}

void CodeGenerator::visitClassNode(ClassNode* node) {
    currentClassName = node->identifier_1->name;
    currentClassInfo = (*classTable)[currentClassName];
    node->visit_children(this);
}

void CodeGenerator::visitMethodNode(MethodNode* node) {
    currentMethodName = node->identifier->name;
    currentMethodInfo = currentClassInfo.methods[currentMethodName];
    std::cout << underscore << currentClassName << "_" << currentMethodName << ":" << std::endl;
    node->visit_children(this);
}

void CodeGenerator::visitMethodBodyNode(MethodBodyNode* node) {
    //prologue
    std::cout << "  # Storing previous frame pointer" << std::endl;
    std::cout << "  push %ebp" << std::endl;
    std::cout << "  # Setting frame pointer as new stack pointer" << std::endl;
    std::cout << "  mov %esp, %ebp" << std::endl;
    std::cout << "  # Allocating memory for local varaiables" << std::endl;
    std::cout << "  sub $" << currentMethodInfo.localsSize << ", %esp" << std::endl;
    node->visit_children(this);
    //epilogue
    if (node->returnstatement)
    {
        std::cout << "  # Return" << std::endl;
        std::cout << "  pop %eax" << std::endl;
    }
    else
    {
        std::cout << "  # NULL Return" << std::endl;
    }
    std::cout << "  mov %ebp, %esp" << std::endl;
    std::cout << "  pop %ebp" << std::endl;
    std::cout << "  ret" << std::endl;   
}

void CodeGenerator::visitParameterNode(ParameterNode* node) {
    node->visit_children(this);
}

void CodeGenerator::visitDeclarationNode(DeclarationNode* node) {
    node->visit_children(this);
}

void CodeGenerator::visitReturnStatementNode(ReturnStatementNode* node) {
    node->visit_children(this);
}

void CodeGenerator::visitAssignmentNode(AssignmentNode* node) {
    node->visit_children(this);
    std::cout << "  # Assignment" << std::endl;
    
    std::cout << "  pop %eax" << std::endl;
    if (currentMethodInfo.variables.find(node->identifier->name)==currentMethodInfo.variables.end())
    {
        std::cout << "  mov " << 8 << "(%ebp), %ebx" << std::endl;
        std::cout << "  mov %eax, " << currentClassInfo.members[node->identifier->name].offset << "(%ebx)" << std::endl;
    }
    else
    {
        std::cout << "  mov %eax, " << currentMethodInfo.variables[node->identifier->name].offset << "(%ebp)" << std::endl;
    }
}

void CodeGenerator::visitCallNode(CallNode* node) {
    node->visit_children(this);
}

void CodeGenerator::visitIfElseNode(IfElseNode* node) {
    int currLabel = currentLabel;
    nextLabel();
    std::cout << "  # IfElse" << std::endl;
    node->expression->accept(this);
    std::cout << "  pop %eax" << std::endl;
    std::cout << "  cmp $1, %eax" << std::endl;
    std::cout << "  jne else" << currLabel << std::endl;
    if (node->statement_list_1)
    {
        std::cout << "  if" << currLabel << ":" << std::endl;
        //first loop
        for(std::list<StatementNode*>::iterator iter = node->statement_list_1->begin(); iter != node->statement_list_1->end(); iter++) {
            (*iter)->accept(this);
        }
    }
    std::cout << "  jmp end" << currLabel << std::endl;
    std::cout << "  else" << currLabel << ":" << std::endl;
    if (node->statement_list_2)
    {

        //second loop
        for(std::list<StatementNode*>::iterator iter = node->statement_list_2->begin(); iter != node->statement_list_2->end(); iter++) {
            (*iter)->accept(this);
        }
    }

    std::cout << "  end"<< currLabel << ":" << std::endl;
}

void CodeGenerator::visitForNode(ForNode* node) {
    // WRITEME: Replace with code if necessary
    int currLabel = currentLabel;
    nextLabel();
    std::cout << "  # ForLoop" << std::endl;
    node->assignment_1->accept(this);
    std::cout << "  loop" << currLabel << ":" << std::endl;
    //for ForLoop
    if (node->statement_list) {
        for(std::list<StatementNode*>::iterator iter = node->statement_list->begin(); iter != node->statement_list->end(); iter++) {
          (*iter)->accept(this);
        }
    }
    node->assignment_2->accept(this);
    node->expression->accept(this);
    std::cout << "  pop %eax" << std::endl;
    std::cout << "  cmp $1, %eax" << std::endl;
    std::cout << "  je loop" << currLabel << std::endl;
}

void CodeGenerator::visitPrintNode(PrintNode* node) {
    node->visit_children(this);
    std::cout << "  push $printstr" << std::endl;
    std::cout << "  call " << underscore << "printf" << std::endl;
}

void CodeGenerator::visitPlusNode(PlusNode* node) {
    node->visit_children(this);
    std::cout << "  # Plus" << std::endl;
    std::cout << "  pop %edx" << std::endl;
    std::cout << "  pop %eax" << std::endl;
    std::cout << "  add %edx, %eax" << std::endl;
    std::cout << "  push %eax" << std::endl;
}

void CodeGenerator::visitMinusNode(MinusNode* node) {
    node->visit_children(this);
    std::cout << "  # Minus" << std::endl;
    std::cout << "  pop %edx" << std::endl;
    std::cout << "  pop %eax" << std::endl;
    std::cout << "  sub %edx, %eax" << std::endl;
    std::cout << "  push %eax" << std::endl;
}

void CodeGenerator::visitTimesNode(TimesNode* node) {
    node->visit_children(this);
    std::cout << "  # Multipy" << std::endl;
    std::cout << "  pop %edx" << std::endl;
    std::cout << "  pop %eax" << std::endl;
    std::cout << "  imul %edx, %eax" << std::endl;
    std::cout << "  push %eax" << std::endl;
}

void CodeGenerator::visitDivideNode(DivideNode* node) {
    node->visit_children(this);
    std::cout << "  # Divide" << std::endl;
    std::cout << "  pop %ebx" << std::endl;
    std::cout << "  pop %eax" << std::endl;
    std::cout << "  cdq" << std::endl;
    std::cout << "  idiv %ebx" << std::endl;
    std::cout << "  push %eax" << std::endl;
}

void CodeGenerator::visitLessNode(LessNode* node) {
    node->visit_children(this);
    int currLabel = currentLabel;
    nextLabel();
    std::cout << "  # Less Than" << std::endl;
    std::cout << "  pop %edx" << std::endl;
    std::cout << "  pop %eax" << std::endl;
    std::cout << "  cmp %edx, %eax" << std::endl;
    std::cout << "  jl lessthan" << currLabel << std::endl;
    std::cout << "  # not less than" << std::endl;
    std::cout << "  push $0" << std::endl;
    std::cout << "  jmp notlessthan" << currLabel << std::endl;
    std::cout << "  " << "lessthan" << currLabel << ":" << std::endl;
    std::cout << "  push $1" << std::endl;
    std::cout << "  " << "notlessthan" << currLabel << ":" << std::endl;
}

void CodeGenerator::visitLessEqualNode(LessEqualNode* node) {
    node->visit_children(this);
    int currLabel = currentLabel;
    nextLabel();
    std::cout << "  # Less Than Or Equal To" << std::endl;
    std::cout << "  pop %edx" << std::endl;
    std::cout << "  pop %eax" << std::endl;
    std::cout << "  cmp %edx, %eax" << std::endl;
    std::cout << "  jle lessthanequal" << currLabel << std::endl;
    std::cout << "  # not less than equal" << std::endl;
    std::cout << "  push $0" << std::endl;
    std::cout << "  jmp notlessthanorequal" << currLabel << std::endl;
    std::cout << "  " << "lessthanequal" << currLabel << ":" << std::endl;
    std::cout << "  push $1" << std::endl;
    std::cout << "  " << "notlessthanorequal" << currLabel << ":" << std::endl;
}

void CodeGenerator::visitEqualNode(EqualNode* node) {
    node->visit_children(this);
    int currLabel = currentLabel;
    nextLabel();
    std::cout << "  # Equals" << std::endl;
    std::cout << "  pop %edx" << std::endl;
    std::cout << "  pop %eax" << std::endl;
    std::cout << "  cmp %edx, %eax" << std::endl;
    std::cout << "  je equal" << currLabel << std::endl;
    std::cout << "  # not equal" << std::endl;
    std::cout << "  push $0" << std::endl;
    std::cout << "  jmp notequal" << currLabel << std::endl;
    std::cout << "  " << "equal" << currLabel << ":" << std::endl;
    std::cout << "  push $1" << std::endl;
    std::cout << "  " << "notequal" << currLabel << ":" << std::endl;
}

void CodeGenerator::visitAndNode(AndNode* node) {
    node->visit_children(this);
    std::cout << "  # And" << std::endl;
    std::cout << "  pop %edx" << std::endl;
    std::cout << "  pop %eax" << std::endl;
    std::cout << "  and %edx, %eax" << std::endl;
    std::cout << "  push %eax" << std::endl;
}

void CodeGenerator::visitOrNode(OrNode* node) {
    node->visit_children(this);
    std::cout << "  # Or" << std::endl;
    std::cout << "  pop %edx" << std::endl;
    std::cout << "  pop %eax" << std::endl;
    std::cout << "  or %edx, %eax" << std::endl;
    std::cout << "  push %eax" << std::endl;
}

void CodeGenerator::visitNotNode(NotNode* node) {
    node->visit_children(this);
    std::cout << "  # Not" << std::endl;
    std::cout << "  pop %eax" << std::endl;
    std::cout << "  xor $1, %eax" << std::endl;
    std::cout << "  push %eax" << std::endl;
}

void CodeGenerator::visitNegationNode(NegationNode* node) {
    node->visit_children(this);
    std::cout << "  # Negation" << std::endl;
    std::cout << "  pop %eax" << std::endl;
    std::cout << "  neg %eax" << std::endl;
    std::cout << "  push %eax" << std::endl;
}
void CodeGenerator::visitMethodCallNode(MethodCallNode* node) {
    int paramSize = 0;
    std::string nameOfObjectClass;
    std::cout << "  # MethodCall" << std::endl;
    std::cout << "  # Pre-Call" << std::endl;
    if (node->expression_list) {
      for(std::list<ExpressionNode*>::reverse_iterator rit = node->expression_list->rbegin(); rit != node->expression_list->rend(); rit++) {
        (*rit)->accept(this);
        paramSize+=4;
      }
    }
    if (node->identifier_2)
    {
        nameOfObjectClass = currentMethodInfo.variables[node->identifier_1->name].type.objectClassName;
        std::cout << "  push " << currentMethodInfo.variables[node->identifier_1->name].offset << "(%ebp)" << std::endl;
        while ((*classTable)[nameOfObjectClass].methods.find(node->identifier_2->name)==(*classTable)[nameOfObjectClass].methods.end() && nameOfObjectClass!="")
        {
            nameOfObjectClass = (*classTable)[nameOfObjectClass].superClassName;
        }
        std::cout << "  call " << underscore << nameOfObjectClass << "_" << node->identifier_2->name << std::endl;
    }
    else
    {
        std::cout << "  push " << 8 << "(%ebp)" << std::endl;
        nameOfObjectClass = currentClassName;
        std::cout << "  call " << underscore << currentClassName << "_" << node->identifier_1->name << std::endl;
    }
    std::cout << "  # Post-Return" << std::endl;
    std::cout << "  pop %ecx" << std::endl;
    std::cout << "  add $" << paramSize << ", %esp" << std::endl;
    std::cout << "  push %eax" << std::endl;

}

void CodeGenerator::visitMemberAccessNode(MemberAccessNode* node) {
    std::cout << "  # Member Access" << std::endl;
    
    if (currentMethodInfo.variables.find(node->identifier_1->name)==currentMethodInfo.variables.end())
    {

        VariableInfo objectInfo = currentClassInfo.members[node->identifier_1->name];
        std::cout << "  mov " << 8 << "(%ebp), %ebx" << std::endl;
        std::cout << "  mov " << objectInfo.offset << "(%ebx), %ebx" << std::endl;
        std::cout << "  push " << (*classTable)[objectInfo.type.objectClassName].members[node->identifier_2->name].offset << "(%ebx)" << std::endl;
    }
    else
    {
        VariableInfo objectInfo = currentMethodInfo.variables[node->identifier_1->name];
        std::cout << "  mov " << objectInfo.offset << "(%ebp), %ebx" << std::endl; 
        std::cout << "  push " << (*classTable)[objectInfo.type.objectClassName].members[node->identifier_2->name].offset << "(%ebx)" << std::endl;
    }
}

void CodeGenerator::visitVariableNode(VariableNode* node) {
    node->visit_children(this);
    std::cout << "  # Variable" << std::endl;
    if (currentMethodInfo.variables.find(node->identifier->name)!=currentMethodInfo.variables.end())
    {
        VariableInfo objectInfo = currentMethodInfo.variables[node->identifier->name];
        std::cout << "  push " << objectInfo.offset << "(%ebp)" << std::endl;
    }
    else
    {
        VariableInfo objectInfo = currentClassInfo.members[node->identifier->name];

        std::cout << "  mov " << 8 << "(%ebp), %ebx" << std::endl;
        std::cout << "  push " << objectInfo.offset << "(%ebx)" << std::endl;
    }
}

void CodeGenerator::visitIntegerLiteralNode(IntegerLiteralNode* node) {
    node->visit_children(this);
}

void CodeGenerator::visitBooleanLiteralNode(BooleanLiteralNode* node) {
    node->visit_children(this);
}


void CodeGenerator::visitNewNode(NewNode* node) {
    std::cout << "  # Malloc For Object" << std::endl;
    std::cout << "  # Push the size of the Object onto Stack" << std::endl;
    std::cout << "  push $" << (*classTable)[node->identifier->name].membersSize << std::endl;
    std::cout << "  # Allocate the memory" << std::endl;
    std::cout << "  call " << underscore << "malloc" << std::endl;
    std::cout << "  # Undo the push" << std::endl;
    std::cout << "  add $4, %esp" << std::endl;
    std::cout << "  push %eax" << std::endl;
}

void CodeGenerator::visitIntegerTypeNode(IntegerTypeNode* node) {
    // WRITEME: Replace with code if necessary
}

void CodeGenerator::visitBooleanTypeNode(BooleanTypeNode* node) {
    // WRITEME: Replace with code if necessary
}

void CodeGenerator::visitObjectTypeNode(ObjectTypeNode* node) {
    // WRITEME: Replace with code if necessary
}

void CodeGenerator::visitNoneNode(NoneNode* node) {
    // WRITEME: Replace with code if necessary
}

void CodeGenerator::visitIdentifierNode(IdentifierNode* node) {
    // WRITEME: Replace with code if necessary
}

void CodeGenerator::visitIntegerNode(IntegerNode* node) {
    std::cout << "  push $" << node->value << std::endl;
}
