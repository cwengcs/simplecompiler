.data
printstr: .asciz "%d\n"
.text
.globl _Main_main
_Class1_getTwoVal:
  # Storing previous frame pointer
  push %ebp
  # Setting frame pointer as new stack pointer
  mov %esp, %ebp
  # Allocating memory for local varaiables
  sub $4, %esp
  # Member Access
  mov 8(%ebp), %ebx
  push 0(%ebx)
  # Assignment
  pop %eax
  mov %eax, -4(%ebp)
  # Variable
  push -4(%ebp)
  # Return
  pop %eax
  mov %ebp, %esp
  pop %ebp
  ret
_Class1_setTwo:
  # Storing previous frame pointer
  push %ebp
  # Setting frame pointer as new stack pointer
  mov %esp, %ebp
  # Allocating memory for local varaiables
  sub $0, %esp
  # Malloc For Object
  # Push the size of the Object onto Stack
  push $4
  # Allocate the memory
  call _malloc
  # Undo the push
  add $4, %esp
  push %eax
  # Assignment
  mov 8(%ebp), %ebx
  pop %eax
  mov %eax, 0(%ebx)
  # MethodCall
  # Pre-Call
  push $0
  push 8(%ebp)
  call _Class2_setVal
  # Post-Return
  pop %ecx
  add $4, %esp
  # NULL Return
  mov %ebp, %esp
  pop %ebp
  ret
_Class1_setTwoToOne:
  # Storing previous frame pointer
  push %ebp
  # Setting frame pointer as new stack pointer
  mov %esp, %ebp
  # Allocating memory for local varaiables
  sub $0, %esp
  # MethodCall
  # Pre-Call
  push $1
  push 8(%ebp)
  call _Class2_setVal
  # Post-Return
  pop %ecx
  add $4, %esp
  # NULL Return
  mov %ebp, %esp
  pop %ebp
  ret
_Class2_setVal:
  # Storing previous frame pointer
  push %ebp
  # Setting frame pointer as new stack pointer
  mov %esp, %ebp
  # Allocating memory for local varaiables
  sub $0, %esp
  # Variable
  push 12(%ebp)
  # Assignment
  mov 8(%ebp), %ebx
  pop %eax
  mov %eax, 0(%ebx)
  # NULL Return
  mov %ebp, %esp
  pop %ebp
  ret
_Main_main:
  # Storing previous frame pointer
  push %ebp
  # Setting frame pointer as new stack pointer
  mov %esp, %ebp
  # Allocating memory for local varaiables
  sub $4, %esp
  # Malloc For Object
  # Push the size of the Object onto Stack
  push $4
  # Allocate the memory
  call _malloc
  # Undo the push
  add $4, %esp
  push %eax
  # Assignment
  pop %eax
  mov %eax, -4(%ebp)
  # MethodCall
  # Pre-Call
  push -4(%ebp)
  call _Class1_setTwo
  # Post-Return
  pop %ecx
  add $0, %esp
  # MethodCall
  # Pre-Call
  push -4(%ebp)
  call _Class1_setTwoToOne
  # Post-Return
  pop %ecx
  add $0, %esp
  # MethodCall
  # Pre-Call
  push -4(%ebp)
  call _Class1_getTwoVal
  # Post-Return
  pop %ecx
  add $0, %esp
  push %eax
  push $printstr
  call _printf
  push $0
  # Return
  pop %eax
  mov %ebp, %esp
  pop %ebp
  ret
